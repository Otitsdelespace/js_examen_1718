var appenTile = function(int_tileNumber){

    var tiles = [];
    for (var i = 0; i < int_tileNumber ; i++) {
        var tile = new Tile();
        tiles.push(tile);
    }

    console.log(tiles);
    var tl = new TimelineMax();
    
    for (var i = 0; i < tiles.length; i++) {
        var tileDisplay = tiles[i].display;
        
        
        //@TODO calcul pour placer les tiles en carré en fonction de leur nombre.
        TweenMax.fromTo(tileDisplay, 0.5, {x:-50,y:-50}, {x:100*i,y:100*i});
        
        //ici peut simplement être fait en css
        //mais si animations plus complexes alors mieux en js
        tileDisplay.addEventListener('mouseover',function (mo){
            TweenMax.to(mo.currentTarget, 0.25,{scale:1.5});
        });
        tileDisplay.addEventListener('mouseout',function (mo){
            TweenMax.to(mo.currentTarget, 0.25,{scale:1});
        });
        //fin du truc possible en css :hover
        
        tileDisplay.addEventListener('click',function (c){
            // tiles.splice(0,1);
            // var tileId = c.currentTarget.num;
            var tileId = c.currentTarget.getAttribute("data-num");
            console.log(tileId);
            tiles.forEach(element => {
                TweenMax.to(element, 0.5, {x:1000,y:-50});
            });
        });
    }
}


var Tile = function(){
    this.display = document.createElement("div");
    this.display.setAttribute("class","tile");
    // this.display.num = Math.floor(Math.random()*13);
    this.display.setAttribute("data-num", Math.floor(Math.random()*13));

    document.body.appendChild(this.display);
}
Tile.prototype.getId = function(){
    return this.id;
}



appenTile(3);



